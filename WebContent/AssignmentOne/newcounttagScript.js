/**
 * 
 */

function generate_table() {
	
	//get all body elements and find number of tags
	let uniqueElementsMap = new Map();
	var tagElements = document.getElementsByTagName("*");
	for (var k = 0; k < tagElements.length; k++) {
		uniqueElementsMap.set(tagElements[k].nodeName, document.getElementsByTagName(tagElements[k].nodeName).length);
	}
	
	var tbl = document.createElement("table");
	var tblBody = document.createElement("tbody");

	//Add tag entries into table
	for (var i = 0; i < 2; i++) {
		var row = document.createElement("tr");

		if (tblBody.getElementsByTagName("th").length > 0) {
			uniqueElementsMap.forEach(function(value, key) {
				var cell = document.createElement("td");
				var cellText = document.createTextNode(value);
				cell.style.padding = "5px";
				cell.style.textAlign = "center";
				cell.appendChild(cellText);
				row.appendChild(cell);
			})

		} else {
			uniqueElementsMap.forEach(function(value, key) {
				var cell = document.createElement("th");
				var cellText = document.createTextNode(key);
				cell.style.padding = "5px";
				cell.style.textAlign = "center";
				cell.appendChild(cellText);
				row.appendChild(cell);
			})
		}

		tblBody.appendChild(row);
	}

	tbl.appendChild(tblBody);
	document.getElementById("newTable").appendChild(tbl);

	//set border
	tbl.setAttribute("border", "3px solid black");
	tbl.setAttribute("border-collapse", "collapse");
	tbl.style.fontSize = "small";
	tbl.style.borderColor = "red";
	
}