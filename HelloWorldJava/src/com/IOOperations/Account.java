package com.IOOperations;

import java.io.Serializable;

public class Account implements Serializable {

	private int accno;
	private String accName;
	private float amount;
	
	public Account(){
		
	}

	public Account(int accno, String accName, float amount) {
		super();
		this.accno = accno;
		this.accName = accName;
		this.amount = amount;
	}

	public int getAccno() {
		return accno;
	}

	public void setAccno(int accno) {
		this.accno = accno;
	}

	public String getAccName() {
		return accName;
	}

	public void setAccName(String accName) {
		this.accName = accName;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}
	
	
}
