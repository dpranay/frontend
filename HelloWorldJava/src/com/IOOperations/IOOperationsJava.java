package com.IOOperations;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class IOOperationsJava {

	public static void main(String args[]) {
		
		
		File file =new File("output.txt");
		if(file.exists()) {
			System.out.println("File Present");
		}
		Account acc =new Account(4,"Pranay",100);
		
		try {
			//V2
			OutputStream out =new FileOutputStream(file);
			
			//V1
			FileOutputStream fout =new FileOutputStream("output.txt");
			BufferedOutputStream bout = new BufferedOutputStream(fout);
			//DataOutputStream dout = new DataOutputStream(bout);
			ObjectOutputStream Oout = new ObjectOutputStream(bout);
			Oout.writeObject(acc);
			//dout.writeUTF("Test Write");
			//dout.close();
			Oout.close();
			bout.close();
			fout.close();
			out.close();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		 try
	        {    
	            FileInputStream fin = new FileInputStream("output.txt");
	            ObjectInputStream oin = new ObjectInputStream(fin); 
	              
	            acc = (Account)oin.readObject(); 

	              
	            System.out.println("Object has been deserialized "); 
	            System.out.println("account No = " + acc.getAccno()); 
	            System.out.println("account name = " + acc.getAccName()); 
	            System.out.println("amount = " + acc.getAmount()); 
	              
	            fin.close(); 
	            oin.close(); 
	        } 
	          
	        catch(Exception e) 
	        { 
	            e.printStackTrace();
	        } 
	}
	
}
